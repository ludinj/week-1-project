export function setErrorFor(input, message) {
  const parent = input.parentElement;
  const pTag = parent.querySelector('p');
  parent.classList.add('error');
  if (parent.classList.contains('success')) {
    parent.classList.remove('success');
  }
  pTag.innerText = message;
}

export function setSuccessFor(input) {
  const parent = input.parentElement;
  parent.classList.add('success');
  if (parent.classList.contains('error')) {
    parent.classList.remove('error');
  }
}

export function isEmail(email) {
  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(email);
}

export function isPhoneNumber(phone) {
  const phoneRegex = /^\d{4}(-?\d{4})?$/;
  return phoneRegex.test(phone);
}

export function isPasswordValid(password) {
  const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[!@#$%^&.*]).{8,50}$/;
  return passwordRegex.test(password);
}

export function isNumber(number) {
  const numberRegex = /^\d+$/;
  return numberRegex.test(number);
}

export function isURL(URL) {
  const urlRegex =
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
  return urlRegex.test(URL);
}

export function setExperienceLevel(element, value) {
  const intValue = parseInt(value);
  if (intValue < 5) {
    const parent = element.parentElement;
    parent.className = 'slider beginner';
    return 'Beginner';
  }
  if (intValue === 5) {
    const parent = element.parentElement;
    parent.className = 'slider competent';
    return 'Competent';
  }
  if (intValue > 5) {
    const parent = element.parentElement;
    parent.className = 'slider expert';
    return 'Expert';
  }
}
